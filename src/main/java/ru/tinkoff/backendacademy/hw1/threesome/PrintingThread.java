package ru.tinkoff.backendacademy.hw1.threesome;

import org.jetbrains.annotations.NotNull;

public class PrintingThread extends Thread {
    private final Integer timesToPrint;
    private final Integer digitToPrint;
    private final ThreesomeOrchestrator orchestrator;

    public PrintingThread(
        int timesToPrint,
        int digitToPrint,
        @NotNull ThreesomeOrchestrator orchestrator
    ) {
        this.timesToPrint = timesToPrint;
        this.digitToPrint = digitToPrint;
        this.orchestrator = orchestrator;
    }

    @Override
    public void run() {
        for (int i = 0; i < timesToPrint; i++) {
            synchronized (orchestrator) {
                while (!isItTimeToPrint()) {
                    try {
                        orchestrator.wait();
                    } catch (InterruptedException ignored) {
                    }
                }

                printTheDigit();
            }
        }
    }

    private boolean isItTimeToPrint() {
        return digitToPrint.equals(orchestrator.state.digit());
    }

    private void printTheDigit() {
        orchestrator.submitDigit(digitToPrint);
    }
}
