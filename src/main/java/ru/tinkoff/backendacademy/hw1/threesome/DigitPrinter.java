package ru.tinkoff.backendacademy.hw1.threesome;

@FunctionalInterface
public interface DigitPrinter {
    void print(int digit);
}
