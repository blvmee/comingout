package ru.tinkoff.backendacademy.hw1.threesome;

import org.jetbrains.annotations.NotNull;

public class ThreesomeOrchestrator {
    public State state = new State(1);
    private final DigitPrinter printer;

    public ThreesomeOrchestrator(@NotNull DigitPrinter printer) {
        this.printer = printer;
    }

    public void start(@NotNull PrintingThread... threads) {
        for (PrintingThread t : threads) {
            t.start();
        }
    }

    public void submitDigit(int digit) {
        synchronized (this) {
            updateState(digit);
            notifyAll();
        }
    }

    private void updateState(int digit) {
        printer.print(digit);
        int nextDigit = state.digit() + 1;
        if (nextDigit > 3) {
            nextDigit = 1;
        }
        state = new State(nextDigit);
    }
}
