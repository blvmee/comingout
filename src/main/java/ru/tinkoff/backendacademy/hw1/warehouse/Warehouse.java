package ru.tinkoff.backendacademy.hw1.warehouse;

import org.jetbrains.annotations.NotNull;

public interface Warehouse<E> {
    @NotNull
    E getResource() throws InterruptedException;
}
