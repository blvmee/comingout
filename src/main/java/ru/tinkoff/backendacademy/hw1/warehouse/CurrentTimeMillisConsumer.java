package ru.tinkoff.backendacademy.hw1.warehouse;

import java.util.Date;

public class CurrentTimeMillisConsumer implements Runnable {
    private final String tag;
    private final Warehouse<Long> warehouse;

    public CurrentTimeMillisConsumer(String tag, Warehouse<Long> warehouse) {
        this.tag = tag;
        this.warehouse = warehouse;
    }

    @Override
    public void run() {
        while (true) {
            try {
                long resource = warehouse.getResource();
                System.out.println(tag + " has consumed " + new Date(resource));
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
