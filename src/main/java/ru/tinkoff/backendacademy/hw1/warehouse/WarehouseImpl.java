package ru.tinkoff.backendacademy.hw1.warehouse;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class WarehouseImpl<E> implements Warehouse<E>, MutableWarehouse<E> {
    private static final Long TERM_TO_WAIT = Long.MAX_VALUE;
    private static final TimeUnit TERM_TIME_UNIT = TimeUnit.MILLISECONDS;

    private final BlockingQueue<E> queue = new LinkedBlockingQueue<>();

    @Override
    public void put(@NotNull E item) throws InterruptedException {
        queue.offer(item, TERM_TO_WAIT, TERM_TIME_UNIT);
    }

    @NotNull
    @Override
    public E getResource() throws InterruptedException {
        return Objects.requireNonNull(
            queue.poll(TERM_TO_WAIT, TERM_TIME_UNIT)
        );
    }
}
