package ru.tinkoff.backendacademy.hw1.warehouse;

public class CurrentTimeMillisProducer implements Runnable {
    private final String tag;
    private final MutableWarehouse<Long> warehouse;
    private final Long delay;

    public CurrentTimeMillisProducer(String tag, MutableWarehouse<Long> warehouse, Long delay) {
        this.tag = tag;
        this.warehouse = warehouse;
        this.delay = delay;
    }

    @Override
    public void run() {
        while (true) {
            try {
                long item = System.currentTimeMillis();
                warehouse.put(item);
                System.out.println(tag + " has produced " + item);
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
