package ru.tinkoff.backendacademy.hw1.warehouse;

import org.jetbrains.annotations.NotNull;

public interface MutableWarehouse<E> extends Warehouse<E> {
    void put(@NotNull E item) throws InterruptedException;
}
