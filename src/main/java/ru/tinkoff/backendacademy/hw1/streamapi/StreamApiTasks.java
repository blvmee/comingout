package ru.tinkoff.backendacademy.hw1.streamapi;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamApiTasks {
    @NotNull
    public List<Integer> squareThenAddTenThenFilterFiveOrSix(@NotNull List<Integer> integers) {
        return integers.stream()
            .map(i -> i * i + 10)
            .filter(i -> i % 10 != 5 && i % 10 != 6)
            .toList();
    }

    @NotNull
    public Map<Integer, Long> buildHistogramOfDuplicates(@NotNull List<Integer> integers) {
        Map<Integer, Long> histogram = integers.stream().collect(
            Collectors.groupingBy(Function.identity(), Collectors.counting())
        );

        histogram.entrySet().removeIf(entry -> entry.getValue() == 1L);

        return histogram;
    }
}
