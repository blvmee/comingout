package ru.tinkoff.backendacademy

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import ru.tinkoff.backendacademy.hw1.threesome.PrintingThread
import ru.tinkoff.backendacademy.hw1.threesome.ThreesomeOrchestrator

private const val TIMES_TO_PRINT_FOR_EACH_THREAD = 6

internal class ThreesomeTest {
    @Test
    fun `test correctness of output 123123123123123123`() {
        val expected = "123123123123123123"
        val builder = StringBuilder()

        val orchestrator = ThreesomeOrchestrator(builder::append)
        orchestrator.start(
            PrintingThread(TIMES_TO_PRINT_FOR_EACH_THREAD, 1, orchestrator),
            PrintingThread(TIMES_TO_PRINT_FOR_EACH_THREAD, 2, orchestrator),
            PrintingThread(TIMES_TO_PRINT_FOR_EACH_THREAD, 3, orchestrator)
        )

        Thread.sleep(100)
        Assertions.assertTrue {
            expected == builder.toString()
        }
    }
}