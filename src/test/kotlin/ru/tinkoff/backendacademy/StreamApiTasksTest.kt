package ru.tinkoff.backendacademy

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import ru.tinkoff.backendacademy.hw1.streamapi.StreamApiTasks

internal class StreamApiTasksTest {

    lateinit var tasks: StreamApiTasks

    @BeforeEach
    fun setUp() {
        tasks = StreamApiTasks()
    }

    @Test
    fun `test first task - map and filter`() {
        val firstCase = listOf(3, 1, 4)
        val firstOutput = tasks.squareThenAddTenThenFilterFiveOrSix(firstCase)
        Assertions.assertTrue {
            firstOutput == listOf(19, 11)
        }

        val secondCase = listOf(1)
        val secondOutput = tasks.squareThenAddTenThenFilterFiveOrSix(secondCase)
        Assertions.assertTrue {
            secondOutput == listOf(11)
        }

        val thirdCase = listOf(2)
        val thirdOutput = tasks.squareThenAddTenThenFilterFiveOrSix(thirdCase)
        Assertions.assertTrue {
            thirdOutput == listOf(14)
        }
    }

    @Test
    fun `test second task - histogram of duplicates`() {
        val firstCase = listOf(1, 2, 3, 4)
        val firstOutput = tasks.buildHistogramOfDuplicates(firstCase)
        Assertions.assertTrue {
            firstOutput == emptyMap<Int, Long>()
        }

        val secondCase = listOf(1, 2, 2, 3, 4, 4, 4, 4)
        val secondOutput = tasks.buildHistogramOfDuplicates(secondCase)
        Assertions.assertTrue {
            secondOutput == mapOf(
                2 to 2L,
                4 to 4L
            )
        }

        val thirdCase = emptyList<Int>()
        val thirdOutput = tasks.buildHistogramOfDuplicates(thirdCase)
        Assertions.assertTrue {
            thirdOutput == emptyMap<Int, Long>()
        }
    }
}