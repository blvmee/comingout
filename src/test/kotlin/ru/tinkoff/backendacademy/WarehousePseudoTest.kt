package ru.tinkoff.backendacademy

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import ru.tinkoff.backendacademy.hw1.warehouse.CurrentTimeMillisConsumer
import ru.tinkoff.backendacademy.hw1.warehouse.CurrentTimeMillisProducer
import ru.tinkoff.backendacademy.hw1.warehouse.MutableWarehouse
import ru.tinkoff.backendacademy.hw1.warehouse.WarehouseImpl

internal class WarehousePseudoTest {
    @Test
    fun `warehouse pseudo test`() {
        val warehouse: MutableWarehouse<Long> = WarehouseImpl()
        Thread(CurrentTimeMillisConsumer("Consumer 1", warehouse)).start()
        Thread(CurrentTimeMillisConsumer("Consumer 2", warehouse)).start()
        Thread(CurrentTimeMillisConsumer("Consumer 3", warehouse)).start()
        Thread(CurrentTimeMillisProducer("Producer", warehouse, 1000L)).start()

        Thread.sleep(10000)
        Assertions.assertTrue {
            true
        }
    }
}